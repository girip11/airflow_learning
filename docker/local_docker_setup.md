# Airflow docker setup

- In production it is recommended to run airflow on kubernetes.

## Running on docker locally

- Minimum 8GB memory should be available to the docker engine.

- LocalExecutor - `docker-compose-local.yaml`
- CeleryExecutor - `docker-compose.yaml`

## Debug

- Use the helper script `airflow.sh` to run commands on the containers.

This helper script can be downloaded using `curl -LfO 'https://airflow.apache.org/docs/apache-airflow/2.2.3/airflow.sh'`

```Bash
./airflow.sh info
./airflow.sh bash
```

## REST API

```Bash
# change the host port 8081 accordingly
ENDPOINT_URL="http://localhost:8081/"
curl -X GET  \
    --user "airflow:airflow" \
    "${ENDPOINT_URL}/api/v1/pools"
```

---

## References

- [running locally using docker](https://airflow.apache.org/docs/apache-airflow/stable/start/docker.html)
