# Useful links

- [Templates and macros in Airflow](https://marclamberti.com/blog/templates-macros-apache-airflow/)
- [How to use timezones in Apache Airflow](https://marclamberti.com/blog/how-to-use-timezones-in-apache-airflow/)
- [Variables in Apache Airflow: The Guide](https://marclamberti.com/blog/variables-with-apache-airflow/)

## Operators

- [Docker operator tutorial](https://marclamberti.com/blog/how-to-use-dockeroperator-apache-airflow/)
- [How to use the BashOperator](https://marclamberti.com/blog/airflow-bashoperator/)
- [The PostgresOperator](https://marclamberti.com/blog/the-postgresoperator-all-you-need-to-know/)

## Kubernetes

- [Kubernetes executor](https://marclamberti.com/blog/airflow-kubernetes-executor/)
- [Running Apache Airflow on a multi-nodes Kubernetes cluster locally](https://marclamberti.com/blog/running-apache-airflow-locally-on-kubernetes/)
