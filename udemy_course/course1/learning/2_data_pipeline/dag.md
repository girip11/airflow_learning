# DAG

- Directed Acyclic Graph
- Nodes are tasks and edges are task dependencies.

## Operators

- Always have a single operator per task in a pipeline
- When a task fails, its easier to retry when it contains only one operator.
- If a task has multiple operators, then even the successful ones will have to be retried again.

3 types of operator

- Action - Execute an action (python operator)
- Transfer - Transfer data
- Sensor - wait for a condition to be met

To write to sqlite database, we need to install sqlite airflow provider. Similar principle holds for any database.

All the installed and available providers can be listed using `airflow providers list`

## Defining task order

- Task order must be defined using `>>` or `<<` operator.

## Testing a task

- `airflow tasks test <dag_id> <task_id> <run_date>` - Test a specific task

## Testing a dag

- `airflow dags test <dag_id> <logical_run_date>` - Test a specific dag

## Dag scheduling

- `start_date`, `schedule_interval` are required to schedule DAG.

## Backfilling and catch up

- DagRun - instance of a DAG scheduled.
- Suppose the DAG is in paused state after creating a schedule for it, the subsequent schedules will be triggered only when we unpause it.
- Once we unpause it, Airflow runs the DAG for all the schedules that were missed due to pausing. This is referred to as **catchup**. This is the default behaviour.
- If we created a DAG to run daily for a start date, then we paused it for two weeks, and later unpause it, 14 dag runs will be triggered as part of catching up.
- We control this behavior by setting `catchup=False` in the code while creatind DAG.

```Python
DAG(
    "user_processing",
    schedule_interval="@daily",
    default_args=default_args,
    catchup=False, # catchup behavior
    render_template_as_native_obj=True,  # This is required to convert to python object
)
```

- All date and time displayed in UI are in UTC. This can be changed to display in local timezone by setting `default_timezone` in the `airflow.cfg`

---

## References

- [Provider packages](https://airflow.apache.org/docs/#providers-packages-docs-apache-airflow-providers-index-html)
- [Airflow operators and hooks reference](https://airflow.apache.org/docs/apache-airflow-providers/operators-and-hooks-ref/index.html)
