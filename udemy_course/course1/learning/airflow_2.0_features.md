# Airflow 2.0

1. Scheduler High availability. Horizontal scaling of scheduler is available. All instance of schedulers are active(active-active model and not master/slave)

2. Dag serialization - storing dag as serialized objects in database instead a DAG folder. `store_serialized_dags` is the configuration to enable this behavior. Scheduler parses the dag from folder dags and stores in the database and the webserver picks them from the DB for displaying. This makes the webserver stateless and improves its performance.

3. DAG versioning. Multiple versions of these DAGs can be stored. Withour versioning earlier the `dag_id` has to be changed everytime which was tedious.

4. REST API - follows openAPI 3.0 specification and accessible through swagger.

5. KEDA autoscaling - For kubernetes. It will autoscale the tasks based on running and tasks in the queue.

6. DAG python API changes. Functional DAGs with the usage of decorators (`@task`)

7. Pluggable XCOM storage engine( say S3, Azure storage etc)
