# Airflow working

## One node architecture

Experimental setup

- Webserver
- Scheduler
- Metastore
- Executor. In single node architecture queue is part of the executor. Queue defines the task ordering.

- Scheduler creates DagRun(workflow instance) and TaskInstance in the metastore and sends the TaskInstance to the executor. Executor has queue that will contain these TaskInstances. Executor is responsible for updating TaskInstance status.

## Multinode Architecture

- Distributed setup. Webserver, scheduler, metastore, executor can all run on different nodes.(master nodes)
- In distributed architecture, the queue is located outside the executor.
- Airflow worker nodes where the tasks will be executed.
