# What is Airflow

- OS platform to programmatically author, schedule and monitor workflows.
- Orchestrator
- Pipelines can be dynamic
- Airflow is scalable
- Airflow UI is powerful.
- Its extensible

## Core components

- Webserver serving the UI (flask based)
- Scheduler - scheduling the pipelines.
- Metastore(database). Default is sqlite. Production recommendations are postgresql/mysql.
- Executor - How/where the tasks will be executed(kubernetes, local etc)
- Worker - Process that executes the task.

- Pipelines/Workflow are DAG(Directed Acyclic Graph)
- Operator - can be of three types action, transfer, sensor operators.
- Action operator ex- python operator, bash operator
- Transfer operator- data transfer
- Sensor operator - wait for something before triggering next task.
- Operator execution becomes a task instance.

Airflow is neither data streaming nor data processing solution.
