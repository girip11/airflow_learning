# Local installation

- [Install airflow specific version with constraints](https://gist.github.com/marclamberti/742efaef5b2d94f44666b0aec020be7c)

OR

- I have followed the instructions available [here](https://airflow.apache.org/docs/apache-airflow/stable/start/local.html). Use my vagrant setup inside the `vagrant` folder.

```Bash
# Airflow needs a home. `~/airflow` is the default, but you can put it
# somewhere else if you prefer (optional)
export AIRFLOW_HOME=~/airflow

# Install Airflow using the constraints file
AIRFLOW_VERSION=2.2.3
# I have used the constraints from https://raw.githubusercontent.com/apache/airflow/constraints-2.2.3/constraints-3.8.txt
pip install "apache-airflow==${AIRFLOW_VERSION}" \
     --constraint "${CONSTRAINT_URL}"

# The Standalone command will initialise the database, make a user,
# and start all components for you.
airflow standalone
```

## Initialization commands

- `airflow --help` lists all the subcommands

```Bash
# airflow db --help
# initializes metastore, airflow folders
airflow db init

# create user for login through webserver
# airflow users create --help
airflow users create --username admin \
    --password admin \
    --firstname admin \
    --lastname admin \
    --role Admin \
    --email admin@example.com

# start the webserver
airflow webserver -p 8080 >/dev/null &
# or
nohup airflow webserver -p 8080 &


# start the scheduler
airflow scheduler >/dev/null &
# or
nohup airflow scheduler &
```

By default airflow uses sqlite database.

## Useful commands

DB commands

```bash
# initialize metastore
airflow db init

# upgrade airflow version
airflow db upgrade

# metastore is cleared. Good for local experiments
airflow db reset
```

DAG list

```bash
airflow dags list

# lists the tasks in a DAG
airflow tasks list <dag_id>

# trigger the dag
airflow dags trigger -e <date> <dag_id>
```

## Webserver UI

- Tree View
- Graph view - current DAG instance status. Log available by clicking on task
- Gantt view - task time. Useful to spot bottlenecks in the DAG pipeline.
