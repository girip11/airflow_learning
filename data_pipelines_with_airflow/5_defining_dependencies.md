# Task dependencies

- Linear dependencies

```python
task1 >> task2
```

- Fan in/Fan out

```python
# fan out
task1 >> [task2, task3]

# fan-in
task1 >> task3
task2 >> task3
# or 
[task1, task2] >> task3
```

## Branching

- We could put the branching logic inside a python operator, but the issue is we won't know from the DAG structure which branch was executed. We need to rely on the logs. Also if the upstream tasks for each branch is different then handling it will be another challenge.

- `BranchPythonOperator` - returns the ID or list of IDs of the downstream task(s).

- When joining multiple branches to a join task, we need to set the trigger rule as only one branch will be executed while other branches will be skipped. By default, join task(fan in) will execute only when all upstream tasks have been completed.
- Default trigger rule for a task is `all_success`. `trigger_rule` parameter can be passed to any operator.
- `from airflow.operators.dummy import DummyOperator` is very helpful often as a join task.

## Conditional tasks

- If a task raises `from airflow.exceptions import AirflowSkipException`, then the downstream tasks are skipped.
- Some use builtin operators
  - `airflow.operators.python.ShortCircuitOperator`
  - `airflow.operators.latest_only.LatestOnlyOperator`

## Trigger rules

- Trigger rule - decide if the task can be executed based on the result of its upstream dependencies.
- Default trigger rule - `all_success`

Other trigger rules are

- `all_failed`
- `none_failed`- None of the upstream tasks should be failed, but can be skipped.
- `none_skipped`
- `all_done` - All upstream tasks should complete execution irrespective of the results. Cleanup tasks are an example.
- `one_failed` - Eager rule. Doesnt require all upstream tasks to be completed. Triggered if any of the upstream tasks fail.
- `one_success` - Eager rule. Triggered as soon as one upstream task is successful.
- `dummy` - Testing purposes. Always triggered irrespective of upstream tasks.

## Sharing data between tasks

- XCom - Cross communication
- XComs can be viewed from the airflow UI.

Publish message

```Python
context["task_instance"].xcom_push(key="key", value="value")
```

Pull the message from XCOM

```Python
# we can also specify dag_id and execution_date
context["task_instance"].xcom_pull(task_ids="task_that_pushed_to_xcom", key="key")
```

- XCom values can be referred in the templates. `'{{ task_instance.xcom_pull(task_ids="",  key= "")}}'`

> Finally, some operators also provide support for automatically pushing XCom values. For example, the `BashOperator` has an option xcom_push, which when set to true tells the operator to push the last line written to stdout by the bash command as an XCom value. Similarly, the `PythonOperator` will publish any value returned from the Python callable as an XCom value.

- We could also fetch XComs pushed by tasks from other DAGs executed at some exection date, though its not recommended.

- XCom should not be used in cases where it might break the task atomicity(passing security token between tasks that could expire).
- Data that's stored in xcom should be serializable and should be of small size.

## Custom XCom backend

- By default xcom backend is the airflow metastore.

```Python
from typing import Any
from airflow.models.xcom import BaseXCom

class CustomXComBackend(BaseXCom):
    @staticmethod
    def serialize_value(value: Any):
        ...

    @staticmethod
    def deserialize_value(result) -> Any:
        ...
```

- With custom xcom backend, we would be able to store the xcom values say in a cloud storage(s3, blob storage) rather than being tied to Airflow's metastore.

## Taskflow API

- `@task` decorator. Function with `@task` decorator returning a value will automatically be pushed to xcom.

- Often used for DAG's with heavy use on python operators.

```Python
from airflow.decorators import task

# no need to pull or push to xcom explicitly with Taskflow API
with DAG(...) as dag:
    ...
    @task
    def train_model():
        model_id = str(uuid.uuid4())
        return model_id

    @task
    def deploy_model(model_id):
        print(f"Deploying model {model_id}")

    model_id = train_model()
    deploy_model(model_id)
```

**NOTE** - Taskflow API is currently restricted to only `PythonOperator`.

- We can use `@dag` decorator to decorate functions.

```Python
import json

import pendulum

from airflow.decorators import dag, task
@dag(
    schedule=None,
    start_date=pendulum.datetime(2021, 1, 1, tz="UTC"),
    catchup=False,
    tags=['example'],
)
def tutorial_taskflow_api():
    """
    ### TaskFlow API Tutorial Documentation
    This is a simple data pipeline example which demonstrates the use of
    the TaskFlow API using three simple tasks for Extract, Transform, and Load.
    Documentation that goes along with the Airflow TaskFlow API tutorial is
    located
    [here](https://airflow.apache.org/docs/apache-airflow/stable/tutorial_taskflow_api.html)
    """
    @task()
    def extract():
        """
        #### Extract task
        A simple Extract task to get data ready for the rest of the data
        pipeline. In this case, getting data is simulated by reading from a
        hardcoded JSON string.
        """
        data_string = '{"1001": 301.27, "1002": 433.21, "1003": 502.22}'

        order_data_dict = json.loads(data_string)
        return order_data_dict
    @task(multiple_outputs=True)
    def transform(order_data_dict: dict):
        """
        #### Transform task
        A simple Transform task which takes in the collection of order data and
        computes the total order value.
        """
        total_order_value = 0

        for value in order_data_dict.values():
            total_order_value += value

        return {"total_order_value": total_order_value}
    @task()
    def load(total_order_value: float):
        """
        #### Load task
        A simple Load task which takes in the result of the Transform task and
        instead of saving it to end user review, just prints it out.
        """

        print(f"Total order value is: {total_order_value:.2f}")
    order_data = extract()
    order_summary = transform(order_data)
    load(order_summary["total_order_value"])
tutorial_taskflow_api()
```

- [Taskflow API documentation](https://airflow.apache.org/docs/apache-airflow/stable/tutorial/taskflow.html)

## Check configuration from CLI

- `airflow config get-value <section> <config_key>`.
- Example to check the type of executor used `airflow config get-value core executor`
