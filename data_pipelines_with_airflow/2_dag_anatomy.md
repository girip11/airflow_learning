# DAG anatomy

- `>>` operator can be used to define the task dependencies.
- Operator in airflow - perform a single piece of work. Operators are like templates for doing something. `PythonOperator`, `HTTPOperator` are some examples.
- DAG execution involves orchestration of execution of collection of operators.
- Task refers to the execution of an operator. (tasks and operators are often used interchangeably)
- Task will encapsulate an operator plus other execution information of the operator.
- Clearing a task will cause it to get scheduled again for execution.
