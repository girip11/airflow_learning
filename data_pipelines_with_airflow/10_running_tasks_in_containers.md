# Running tasks in containers

- With logic encapsulated into operators, each operator will have its own python dependencies.
- These dependencies have to be made available to airflow scheduler(which parses the DAG) as well as to the worker which will execute the dag tasks.
- Hence when we containerize the logic, we always needs to interact with only one type of operator and all the dependencies can be bundled into the container.
- With logic containerized it will be much more easier to test the logic.
- We can use kubernetes to run the airflow tasks using `KubernetesPodOperator`
