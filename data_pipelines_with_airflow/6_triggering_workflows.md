# Triggering Workflows

## Sensors

- Sensors periodically poll for a certain condition to be met. If false it will try again till the condition is met or timeout is reached.

- `PythonSensor` - returns either true or false after execution of the python callable. This helps us to check for custom conditions.

- Sensor timeout refers to the maximum time the sensors are allowed to run(within this timeout period, sensors can be run to poll for the condition many times). Default sensor timeout is 7 days.

- Each DAG has a limit of the number of tasks it can run concurrently. This can be set with the `concurrency` argument passed while instantiating the DAG.

- In the airflow configuration, dag concurrency can be set with either `dag_concurrency`(`prior to 2.2.0`) and `core.max_active_tasks_per_dag`.

- Without proper timeout configured and frequent schedules, we could hit sensor deadlock(sensor tasks keep running for long while new schedules create more sensor tasks and hence maxing out the number of tasks active for that particular dag).
- Sensors can run in poke or reschedule mode. In poke mode, the resource consumed for running the sensor is allocated permanently and is blocked till sensor completes.
- In reschedule mode the resources are used only during execution(poking) and then the task slot is released.

## DAG concurrency and parameters to tune

- `AIRFLOW__CORE__PARALLELISM` -  total tasks that can be executed simultaneously per scheduler.
- `AIRFLOW__CORE__MAX_ACTIVE_TASKS_PER_DAG`
- `max_active_runs_per_dag` - active runs per dag. Default 16
- `dag_file_processor_timeout` - How long `DagFileProcessor` can run processing a DAG.
- `dagbag_import_timeout`

We have parameters for scheduler as well. We also have settings that can be set at DAG or task level.

## Triggering other DAGs

- `TriggerDagRunOperator` can be used to trigger a DAG from another DAG.
- DAG `run_id` either starts with `scheduled__`, `backfill__` or `manual__`

> Clearing tasks in a DAG, including a TriggerDagRunOperator, will trigger a new
DAG run instead of clearing the corresponding previously triggered DAG runs

## Polling state of other DAGs

- Airflow manages dependencies between tasks in a dag but not inter dag dependencies.

- `ExternalTaskSensor` - pokes the state of tasks in the other DAGs. When the schedule interval of two dags dont match, we could use `execution_delta` which is a `datetime.timedelta` object. By default, the checking dag always checks the state of the other dag on the same execution date(which is a timestamp). If the dag to check was launched earlier in the day or the previous day, then using `external_delta` we can make the second dag with a later execution date still able to check the state of previously launched dags.

- REST API, CLI (useful from CI/CD pipelines or called from Serverless/AWS lambda) and UI can all be used to launch dags.

```bash
airflow dags trigger --conf '{"setting1": "value"}' <dag_name>
```

**NOTE** - DAGs that rely on a conf to run cant be scheduled.

---

## References

- [Scaling Airflow](https://www.astronomer.io/guides/airflow-scaling-workers/)
