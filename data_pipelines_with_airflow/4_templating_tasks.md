# Templating in Airflow

- Airflow uses the `pendulum` library for datetime. The time related are templating variables like `execution_date` are pendulum datetime objects.
- Every operator has parameters that are templatable. Each operator defines `template_fields`.

```Python
# snippet to dump all the variables available for templating
import airflow.utils.dates
from airflow import DAG
from airflow.operators.python import PythonOperator

dag = DAG(
    dag_id="chapter4_print_context",
    start_date=airflow.utils.dates.days_ago(3),
    schedule_interval="@daily",
)

def _print_context(**kwargs):
    print(kwargs)

print_context = PythonOperator(
    task_id="print_context",
    python_callable=_print_context,
    dag=dag,
)
```

Some of the important task context variables are (for exhaustive list refer the book)

- `conf` - `airflow.configuration.AirflowConfigParser` instance
- `dag` - `DAG` instance
- `dag_run` - `DagRun` instance
- `run_id` - DAG run id
- `task` - current operator object
- `task_instance` or `ti` - `TaskInstance` object
- `task_instance_key_str` - unique identifier of the current task instance
- `test_mode` - airflow test mode
- `var` - Helper objects for dealing with airflow variables

## Python operator

```Python
# Here context is a dictionary of all the airflow variables that would 
# be avaiable for templating
def python_op_callable(**context):
    print(context["execution_date"])

# we could also be explicitly mentioning the arguments we would
# require. Those will not be captured in the context rather passed
# separately
def python_op_callable(conf, execution_date, **context):
    print(execution_date)
    print("execution_date" in context) # should be False
```

The airflow variables that are explicitly mentioned as parameters are passed and are removed from the `context` parameter.

## Inspecting templated arguments

- Airflow UI can be used to debug. In the airflow UI, once the task is triggered, we have the `Rendered Template` option to check if the template values were correctly passed.

- Using airflow CLI - most convenient as we can render the value anytime. No need to schedule a task to view the value. This doesnot trigger the task or store anything in the metastore.

```bash
airflow tasks render <dag_id> <task_id> <execution_date>
```

## Task communication

- Using xcom(builtin), which uses Airflow metastore
- Using disk(file) or external database.

Using xcom, the resulting object of the task should be serializable. We should only store small objects using xcom.

If the data is large, one possible mechanism is to write the data to a disk(maybe as a blob in the cloud storage) and store only the blob name in Xcom.

Airflow provider packages provide operators for many things.

Connections to database can be persisted in the airflow metastore once we create the **airflow connection**. We can either use UI or CLI for this.

```bash
airflow connections add \
--conn-type postgres \
--conn-host localhost \
--conn-login postgres \
--conn-password mysecretpassword \
my_postgres
```

- Files that will be given as input to an operator can also be templated. To check what file extensions are supported, `template_ext` attribute of the operator canbe inspected.
- Jinja requires the path of the files that should be templated. We need to pass `template_searchpath` parameter to the DAG object to enable file templating.

- Operator - what needs to be done
- Hooks - how it should be done. Hooks are used inside operators.
