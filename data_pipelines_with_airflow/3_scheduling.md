# Scheduling in Airflow

Scheduling params

- `start_date`
- `schedule_interval`
- `end_date` - optional

First execution - after the elapse of `start date + interval`.

## Cron expressions

```text
*               *               *           *               *
minute(0-59)  hour(0-23)    day(1-31)   month(1-12)     weekday(0-6)

# Weekday Sunday to Saturday
# Range of values supported. `1,2` and `1-5`
# * - if we don't care about specific values for that field
```

Scheduling macros for commonly used cron expressions.

- `@once`
- `@hourly`
- `@daily`
- `@weekly`
- `@monthly`
- `@yearly`

- [Cron expression validation](https://crontab.guru)

## Frequency based intervals

- Using `datetime.timedelta` we can define frequency based intervals like every three days.

```Python
dag = DAG(
    dag_id="04_time_delta",
    schedule_interval=dt.timedelta(days=3),
    start_date=dt.datetime(year=2019, month=1, day=1),
    end_date=dt.datetime(year=2019, month=1, day=5),
    )
```

## Dynamic Time parameters for tasks

- `execution_date` - Timestamp of the start time of the DAG's current execution.
- `next_execution_date` - Timestamp of next schedule interval.
- `previous_execution_date` - Timestamp of previous schedule interval.

We can use these variables using templating(jinja). Syntax `"{{ execution_date.strftime('%Y-%m-%d') }}"`

Different formats of `execution_date`

- `ds` (YYYY-MM-DD), `ds_nodash`(YYYYMMDD)
- `prev_ds`, `prev_ds_nodash`
- `next_ds`, `next_ds_nodash`

## Backfilling

- Backfilling - start date in the past
- Task is scheduled for all the schedule intervals upto current time by default
- Setting catchup to false will ensure only most recent interval getting scheduled.
- can be used to collect historical data or rerun changed code on the historical data.

## Good practices for designing tasks

- Atomicity - Tasks should either succeed or fail.
- Idempotency - Same output for the same input.
