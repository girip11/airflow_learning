# Data pipelines with Apache Airflow

Notes from the book [Data Pipelines with Apache Airflow](https://www.manning.com/books/data-pipelines-with-apache-airflow)

- [Airflow documentation](https://airflow.apache.org/docs/apache-airflow/stable/concepts/index.html)

## Developing locally

- To test the individual tasks locally without an airflow deployment, we need to do the following.

```bash
export AIRFLOW_HOME="<airflow home directory>"
export AIRFLOW__CORE__DAGS_FOLDER="<dags directory path>"

# ensure that  you are not connected to any external database by setting
# AIRFLOW__CORE__SQL_ALCHEMY_CONN 
# we would be using sqlite database for development
airflow db init # initialized sqlite database in AIRFLOW_HOME

# this command executes specific tasks locally
airflow tasks test <dag_is> <task_id> <execution_date>
```

Sqlite database can be inspected with DBeaver.
