# Communicating with external systems

- External system - any service/system external to Airflow. For instance a postgres database.

- Testing the task locally

```bash
airflow tasks test --env-vars <env_vars> <dag_id> <task_id> <execution_date>
```

> airflow tasks test exists for running and verifying a single task and does not record any state in the database; however, it does require a database for storing logs, and therefore we must initialize one with `airflow db init`.

We need to have a local database for testing the tasks locally. Sqlite database can be examined with tools like beekeeper-studio or DBeaver.

## Outsourcing heavy computations

- Heavy resource intensive computations can be offloaded to systems like spark using operators from the airflow spark provider.
- There are other operators like `KubernetesPodOperator`, `DockerOperator`
