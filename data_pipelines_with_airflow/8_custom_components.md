# Building custom components in Airflow

## Custom Hook

- We can create connection to external systems(credentials) from the airflow UI

```Python
from airflow.hooks.base_hook import BaseHook

class CustomHook(BaseHook):
    def __init__(self, conn_id):
        ...
```

> Most Airflow hooks are expected to define a `get_conn` method, which is responsiblefor setting up a connection to an external system.

- Recommendation is not to call the `get_conn` method from outside. Don't consider/treat it as a public API method as its tied to how the hook connects to the external system.

- `BaseHook.get_connection` method can be used to fetch the connection details from the airflow metastore.

## Building Custom operator

```Python
from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults

# apply defaults applies the default args that we pass to the DAG
class CustomOperator(BaseOperator):
    # these parameters passed to init can be templated with airflow variables
    template_fields = ("start_date", "end_date", "output_path")

    @apply_defaults
    def __init__(self, conn_id, start_date, end_date, output_path, **kwargs):
        super.__init__(self, **kwargs)
        self._conn_id = conn_id

    # context is dict of airflow variables
    def execute(self, context):
        ...
```

## Building custom sensors

```python
from airflow.sensors.base import BaseSensorOperator

class CustomSensor(BaseSensorOperator):
    def poke(self, context) -> bool:
        ...
```
