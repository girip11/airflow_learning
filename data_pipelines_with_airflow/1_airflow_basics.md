# Airflow components

- Webserver - Management, monitoring UI
- Scheduler - parses DAGs and schedules them
- Worker - execute tasks
- Metastore - store parsed DAGs serialized form, tasks to schedule, task results
- In distributed environment for task queue redis is used.
