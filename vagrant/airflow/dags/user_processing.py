import json
from datetime import datetime
from typing import Any, Dict, Mapping, cast

from airflow.models.dag import DAG
from airflow.models.taskinstance import TaskInstance
from airflow.operators.bash import BashOperator
from airflow.operators.python import PythonOperator

# from airflow.providers.http.operators.http import Respo
from airflow.providers.http.operators.http import SimpleHttpOperator
from airflow.providers.http.sensors.http import HttpSensor
from airflow.providers.sqlite.operators.sqlite import SqliteOperator
from requests import Response

# Workflow summary
# =========================
# 1. Create users table in sqlite
# 2. Check if user api exists
# 3. fetch the user details from the http api
# 4. Create user using python
# 5. Write the user to the users table

# Arguments common to all the tasks/operator in the pipeline
default_args = {"start_date": datetime(2022, 2, 18), "retries": 1}


def check_user_api_response(response: Response) -> bool:
    return response.status_code == 200


def filter_user_api_response(response: Response) -> Any:
    return json.loads(response.text)


# here we extract the ti from the context
def process_user_alt(**kwargs: Mapping[str, Any]) -> Mapping[str, str]:
    ti = cast(TaskInstance, kwargs["ti"])
    user = ti.xcom_pull(task_ids="extracting_user")
    if user.get("results", None) is None:
        raise ValueError("User data is missing")

    user_data = user["results"][0]
    return {
        "email": user_data["email"],
        "firstname": user_data["name"]["first"],
        "lastname": user_data["name"]["last"],
        "username": user_data["login"]["username"],
        "password": user_data["login"]["password"],
        "country": user_data["location"]["country"],
    }


# share data between task instances using xcom_pull
# When return is used, the value is automatically stored in DB
# for xcom
def process_user(user: Mapping[str, Any]) -> Mapping[str, str]:
    if user.get("results", None) is None:
        raise ValueError("User data is missing")

    user_data = user["results"][0]
    # user_df = json_normalize(
    #     {
    #         "email": user_data["email"],
    #         "firstname": user_data["name"]["first"],
    #         "lastname": user_data["name"]["last"],
    #         "username": user_data["login"]["username"],
    #         "password": user_data["login"]["password"],
    #         "country": user_data["location"]["country"],
    #     }
    # )

    # # Writing to file is just to make use of bash operator
    # # We could have written to the database using the python operator itself.
    # user_df.to_csv("/tmp/processed_user.csv", index=None, header=None)
    return {
        "email": user_data["email"],
        "firstname": user_data["name"]["first"],
        "lastname": user_data["name"]["last"],
        "username": user_data["login"]["username"],
        "password": user_data["login"]["password"],
        "country": user_data["location"]["country"],
    }


with DAG(
    "user_processing",
    schedule_interval="@daily",
    default_args=default_args,
    catchup=False,
    render_template_as_native_obj=True,  # This is required to convert to python object
) as dag:
    # defined tasks/operators
    # always have one operator per task in the pipeline
    creating_table = SqliteOperator(
        task_id="table_creation",
        sqlite_conn_id="db_sqlite",
        sql="""
        CREATE TABLE IF NOT EXISTS users (
            email TEXT NOT NULL PRIMARY KEY,
            firstname TEXT NOT NULL,
            lastname TEXT NOT NULL,
            username TEXT NOT NULL,
            password TEXT NOT NULL,
            country TEXT NOT NULL
        );
        """,
    )

    is_api_available = HttpSensor(
        task_id="is_api_available", http_conn_id="user_api", endpoint="api/"
    )

    extracting_user = SimpleHttpOperator(
        task_id="extracting_user",
        http_conn_id="user_api",
        endpoint="api/",
        method="GET",
        response_check=check_user_api_response,
        response_filter=filter_user_api_response,
        log_response=True,
    )

    # processing_user = PythonOperator(
    #     task_id="processing_user",
    #     python_callable=process_user_alt,
    # )
    # Airflow is able to pick up the task dependency automatically
    # using the output reference.
    processing_user = PythonOperator(
        task_id="processing_user",
        python_callable=process_user,
        op_kwargs={"user": extracting_user.output},
        # can use jinja template in op_args, op_kwargs
        # op_kwargs={"user": "{{ti.xcom_pull('extracting_user')}}""},
    )

    # Jinja templating works. To know which parameters support the templating
    # look at the documentation. Every operator exposes attributes like
    # template_fields, template_field_renderers etc
    # In case of BashOperator bash_command and env are templatable.
    storing_user = BashOperator(
        task_id="storing_user",
        bash_command="""sqlite3 "/home/vagrant/airflow/airflow.db" <<EOF
INSERT INTO users(email, firstname, lastname, username, password, country)
VALUES ('${email}', '${firstname}', '${lastname}', \
'${username}', '${password}', '${country}')
EOF""",
        env=cast(Dict[str, str], "{{ti.xcom_pull('processing_user')}}"),
    )

    # Time to define the task ordering. >> is used to define ordering
    # VERY IMPORTANT
    # order will also be picked up from passing output.
    # but its a good practice to be explicit
    creating_table >> is_api_available >> extracting_user >> processing_user >> storing_user
