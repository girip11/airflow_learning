from datetime import datetime

from airflow.models.dag import DAG
from airflow.operators.python import PythonOperator

# from airflow.providers.http.operators.http import Respo

default_args = {"retries": 1}


def dump_context(**context) -> None:
    print("execution_date" in context)
    print(context["execution_date"])


def dump_context_explicit(conf, execution_date, **context) -> None:
    print("execution_date" in context)
    print("conf" in context)
    print(conf, type(conf))
    print(execution_date)


def debug_template(execution_year, dag_conf, **context):
    print(execution_year)


with DAG(
    "python_op_example",
    schedule_interval="@daily",
    default_args=default_args,
    start_date=datetime.utcnow(),
    catchup=False,
    render_template_as_native_obj=True,  # This is required to convert to python object
) as dag:

    simple_task = PythonOperator(task_id="simple_task", python_callable=dump_context, dag=dag)

    simple_task2 = PythonOperator(
        task_id="simple_task_explicit", python_callable=dump_context_explicit, dag=dag
    )

    debug_templating_task = PythonOperator(
        task_id="debug_templating_task",
        python_callable=debug_template,
        op_args=["{{data_interval_start.year}}"],
        op_kwargs={"dag_conf": "{{ conf }}"},
        dag=dag,
    )

    simple_task >> simple_task2
    simple_task2 >> debug_templating_task
